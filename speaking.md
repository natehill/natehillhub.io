---
layout: page
title: Speaking
permalink: /speaking/
---
<br />

* __2013, The Future of Libraries Conference, Vancouver CA__<br />
Transforming Public Libraries

* __2013, LITA Forum, Louisville KY__<br />
Keynote: Transforming the Library in Chattanooga TN

* __2013, Carnegie UK Trust, Dunfermline Scotland__<br />
Chattanooga Public Library’s 4th Floor and the Future of Libraries

* __2013, Creative Making in Libraries & Museums Symposium, Toronto iSchool, Canada__<br />
The Beta Space: Prototyping Library Services in Chattanooga

* __2013, NextLibrary Conference, Aarhus Denmark__<br />
Iterative Service Design & Creative Citizenship

* __2012, Library Journal Directors Summit, Los Angeles CA__<br />
The 4th Floor of Chattanooga Public Library

* __2012, SXSW Interactive, Austin, TX__<br />
Making Stories: Libraries and Community Publishing

* __2012, Public Library Association National Conference, Philadelphia PA__<br />
Designing and Building a Social Library Website

* __2011, Digital Library Federation / Council for Library and Information Resources Forum, Baltimore MD__<br />
What a National Digital Library Means For Public Libraries

* __2011, Library Journal eBook Summit__<br />
Library Service Models in the Digital Transition: From Read to Read/Write

* __2011, Internet Librarian, Monterey CA__<br />
Designing Mobile Experiences

* __2011, Computers in Libraries, Washington DC__<br />
Designing Digital Spaces for Positive User Experiences: UX4Lib

* __2010, Handheld Librarian Conference__<br />
Augmented Reality 101

* __2009, NextLibrary Conference, Aarhus Denmark__<br />
The Library Outpost

##Consulting Clients

* __Harvard University, Berkman Center for Internet and Society__<br />Use case scenarios for Digital Public Library of America planning process
* __Public Library Association (PLA)__<br />Blog Manager
* __Influx Web UX Consulting__<br />“One Pager” open source web page template for public libraries
* __County of Los Angeles Public Library, CA__<br />Library future visioning workshop
* __The Dayton Metro Library, OH__<br />Branding, UX, and futures workshops for $186 million building project

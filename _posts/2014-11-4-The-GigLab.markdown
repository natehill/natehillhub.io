---
layout: proj
title:  "GigLab"
date:   2014-11-3 10:27:34
category: project
images:
 - img: /img/natesite-giglab.jpg
---

The [GigLab](http://blog.giglab.io/), located on the [4th Floor](https://www.flickr.com/photos/chattlibrary/sets/72157631269756074/) of the [Chattanooga Public Library](http://chattlibrary.org/), provides access to gigabit connected resources for the purposes of startup development and education.  It was launched thanks to a generous grant from the [Mozilla Foundation](https://www.mozilla.org/en-US/foundation/). The GigLab is operated and administered by myself, [Jake Brown](https://twitter.com/jkaebrwon), [Andrew Rodgers](https://twitter.com/acedrew), [Sean Brewer](https://twitter.com/seabre), and [James McNutt](https://twitter.com/jmcnuttcase).

The public library gets its gigabit internet service from the [Electric Power Board](https://www.epb.net/) of Chattanooga (EPB).  EPB is Chattanooga’s municipal electric power distributor and fibre optic based communications provider.  EPB owns and operates the largest network of its kind in the US; the fibre optic network is both the communications backbone for EPB’s smart electric power grid and the foundation for EPB’s advanced communications services.  Because of our unique infrastructure in Chattanooga, we have been able to partner with EPB to offer a dedicated public network and equipment stack for anyone with a library card to run experiments.

---
layout: proj
title:  "The 4th Floor"
date:   2014-11-1 10:27:34
category: project
images:
 - img: /img/natesite-4thfloor.jpg
 - img: /img/natesite-4thfloor-8.jpg
 - img: /img/natesite-4thfloor-1.jpg
 - img: /img/natesite-4thfloor-2.jpg
 - img: /img/natesite-4thfloor-3.jpg
 - img: /img/natesite-4thfloor-4.jpg
 - img: /img/natesite-4thfloor-5.jpg
 - img: /img/natesite-4thfloor-6.jpg
 - img: /img/natesite-4thfloor-7.jpg
---

When I arrived in Chattanooga, TN in July of 2012, the fourth floor of the downtown public library building was 12,000 square feet of mess. Since the brutalist concrete building opened in 1976, nobody had properly disposed of unwanted items, instead they were moved to the attic of the building presumedly to be stored for eternity. In just one year, I organized and led an effort to clean up this space and give it back to the people of Chattanooga.

The 4th floor is a public laboratory and educational facility with a focus on information, design, technology, and the applied arts. It is a responsive community platform that features services shaped by the creative and technological needs of the citizens of Chattanooga. The more than 12,000 sq foot space hosts equipment, expertise, programs, events, and meetings that work within this scope. While traditional library spaces support the consumption of knowledge by offering access to media, the 4th floor is unique because it supports the production, connection, and sharing of knowledge by offering access to tools and instruction.

The 4th Floor also functions as the library’s “beta space”, a public space where we prototype new services and solutions before implementing them across the library building and system. This iterative approach to service design allows us to rapid prototype new ideas in a risk-friendly environment.  On the 4th Floor, you’ll find library staff solving library service delivery problems right alongside community members using the tools and space to solve their own problems. Those tools include vinyl cutters, laser cutters, 3d printers, and more. In the early days of transformation, the 4th Floor was only open during special events. Now it is open afternoons and evenings daily.

Now, after two years of operation and experimentation, we’ve learned that certain genres of activities and initiatives on the 4th Floor tend to cluster together to form their own “labs” or “studios”. The emergent labs are not defined by physical boundaries, but rather intersect, overlap, and affect one another with their activities. Experience has shown us that collocation of different labs actually strengthens the platform as a whole. Currently, with varying degrees of participation and interest, the 4th Floor supports four distinct labs: the Civic Lab, the Maker Lab, the Art Lab, and the most recent addition: our public GigLab.

The 4th Floor is an ongoing experiment.  Ultimately, what is special about the 4th Floor isn’t any specific gigabit application, gadget, or any particular data set. What is special is that all of these diverse, contemporary, and relevant resources are made available to any Chattanooga resident who possesses a library card.
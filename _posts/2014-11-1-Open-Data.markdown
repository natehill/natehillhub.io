---
layout: proj
title:  "Open Data"
date:   2014-11-1 10:27:34
category: project
images:
 - img: /img/natesite-open.jpg
 - img: /img/natesite-open-1.jpg
 - img: /img/natesite-open-2.jpg
 - img: /img/natesite-open-3.jpg
 - img: /img/natesite-open-4.jpg
---

In partnership with the [office of Mayor Andy Berke](http://www.chattanooga.gov/mayors-office) and [Open Chattanooga](http://openchattanooga.com/), the Chattanooga Public Library offers access to open [data collections as a library service](https://data.chattlibrary.org/).  While many cities have open data portals, we are currently the only city to use the public library as the repository for these data collections.  By treating open data as just another library collection, we are able to expand the scope of the collections beyond what other cities might offer.  The library plays a responsive, curatorial role offering data from a broad scope of sources and has a full time [Open Data Specialist](https://twitter.com/seabre) on staff.

This effort has also given open data a physical space in the community as well.  The Chattanooga Public Library’s [4th Floor](https://www.flickr.com/photos/chattlibrary/sets/72157631269756074/) can be thought of as home to these efforts.  The 4th Floor is the architectural equivalent of an open data platform; it is a flexible community-driven space for innovation, collaboration, and the sharing of knowledge.  The 4th Floor has served as host to hackathons like the national day of civic hacking or other events like the upcoming citycamp.  We’ve anchored these digital collections in a space that is not designed to house them, instead it is designed for people to learn about and use them.

This project launched thanks to funding from the [Knight Foundation Community Information Challenge](http://www.knightfoundation.org/kcic/) and the [Benwood Foundation](http://www.benwood.org/). The grant aligned city hall, the library, and citizen-driven efforts as a collaborative, cohesive partnership. Our work is ongoing.
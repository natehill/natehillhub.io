---
layout: proj
title:  "The Library Outpost"
date:   2014-10-29 10:27:34
category: project
images:
 - img: /img/natesite-outpost.jpg
---

In 2006, I began a research project at Pratt Institute describing a flexible, scalable storefront library service model for connected cities using Brooklyn’s Dumbo neighborhood as a target site. This library semi-popup was programmed to have no locally hosted book collection.
                                  		
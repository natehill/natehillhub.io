---
layout: proj
title:  "Greenpoint Poetry Sites"
date:   2014-10-29 10:27:34
category: project
images:
 - img: /img/natesite-gps.jpg
 - img: /img/natesite-gps-1.jpg
 - img: /img/natesite-gps-2.jpg
 - img: /img/natesite-gps-3.jpg
---

In 2008, before QR codes were lame, before there were Foursquare and Facebook checkins, there was Greenpoint Poetry Sites. These were place-based community poems, launched and edited on your mobile phone.
                                             		
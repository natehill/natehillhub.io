---
layout: proj
title:  "DPLA Use Cases"
date:   2014-10-30 10:27:34
category: project
categories: projects
images:
 - img: /img/natesite-dpla.jpg
---

During the two-year planning phase for the Digital Public Library of America, I served as a co-chair of the Audience & Participation workstream. Since the launch of the DPLA, I have been acting as a co-chair of the Marketing & Outreach committee. During this planning phase, there was broad consensus on the defining principles describing the DPLA, but because it did not yet exist as software it was sometimes difficult to translate these principles into features for the benefit of end-users.

I was fortunate to have an opportunity to lead the development of ten unique use cases that were driven by community input into what kind of problems the DPLA might be able to solve. Even now, one year after the launch of the DPLA in April 2012, the use cases remain relevant and provide future direction for the platform, portal and third-party applications.
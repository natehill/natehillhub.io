---
layout: proj
title:  "Hyperaudio #CHA"
date:   2014-11-3 10:27:34
category: project
images:
 - img: /img/natesite-hyperaudio.jpg
---

Thanks to the [Mozilla Gigabit Community Fund](https://www.mozilla.org/en-US/gigabit/), the [Chattanooga Public Library](http://chattlibrary.org/) was granted an opportunity to work with [Hyperaudio](http://hyperaud.io/) to create our own localized version their web remixing platform called [Hyperaudio Chattanooga](http://chattanooga.hyperaud.io/).

Hyperaudio and [Hyperaudio Pad](http://chattanooga.hyperaud.io/pad/) are a powerful set of tools that allow users to remix audio and video content right in the web browser. Mixing video content by building user-facing hooks into its transcript makes the mixing experience easy and intuitive.  Instead of fast-forwarding through a long video to find the part you are looking for, text snippets are easy to isolate and manipulate in a web browser.  

The story of this project began with the discovery of a box of almost 20 videotapes from the early 1980s that were found deteriorating in the Chattanooga Public Library’s local history department. The videotapes had been given to the public library by [WTCI](http://wtcitv.org/), the local public television station, many years ago. On the tapes were a series of oral history interviews with local characters, some of whom were significant community figures. Nobody had seen these interviews in 30 years, as they’d been relegated to a dim, inaccessible, existence in a musty back room. In preparation for our experiment with Hyperaudio, the tapes were digitized, edited, and transcribed. The interviews offered amazing insights into what life was like in Chattanooga around the beginning of the 20th century as well as in the 1970s: we had discovered a real gem.

Once this content was made available on the Hyperaudio platform it was incredibly easy to make fun, engaging, remixed narratives. Notably, I found that as I mined the text and the footage for moments to best express the story I wanted to tell in my own creation, I developed a much more intimate understanding of the content itself. I had unknowingly been engaged in an active learning process. When I edited and remixed the content to make my own creation, I was learning by doing; I was engaged in a personal, creative, learning process that allowed me to experience the content relative to my own needs.

Ultimately, we were able to engage other content partners in Chattanooga Hyperaudio.  You’ll find the [Hunter Museum of American Art](http://www.huntermuseum.org/) has their [Art + Issues](http://www.huntermuseum.org/artissues/) series available there, as well as [Chattanooga Pecha Kucha](http://www.pechakucha.org/cities/chattanooga).  Chattanooga Hyperaudio gives Chattanoogans an opportunity to remix and own their own stories about their own city.





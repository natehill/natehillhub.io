---
layout: proj
title:  "Library Lab"
date:   2014-10-29 10:27:34
category: project
images:
 - img: /img/natesite-liblab.jpg
---

The Library Lab is an open source, portable, modular solution for content creation in libraries. This was an entry in a design competition in 2010. The collaboration included myself, Noll & Tam Architecture, Matthew Williams Design, Wikimedia DC, and others.
                       		
---
layout: post
title:  "Hello World"
date:   2014-11-3 11:51:34
category: blog
categories: 
description: the preview field is still here
---
Hello there.  Welcome to this new site.  It is made with the Jekyll framework, hosted as a GitHub page, is laid out using Twitter Bootstrap, and the blog uses Disqus for comments.  For me, the build was an experiment using new tools.  For you, this is where you can check in on what I’m writing about, thinking about, or projects that I’ve engaged in.  

Thanks.  See you again soon.

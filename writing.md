---
layout: page
title: Writing
permalink: /writing/
---
<br />

* __Next Century Cities, 11.2014__<br />
[Shaping the 21st Century Public Library: Chattanooga’s Story](http://nextcenturycities.org/2014/11/12/shaping-the-21st-century-public-library-chattanoogas-story/)

* __Transcript, Code for America Summit 2015, 9.2014__<br />
[Chattanooga Public Library and Open Data](https://medium.com/@natenatenate/chattanooga-public-library-and-open-data-a6e3724afaba)

* __Urban Omnibus, 7.2014__<br />
[Precedents for Experimentation: Talking Libraries with Shannon Mattern and Nate Hill](http://urbanomnibus.net/2014/07/precedents-for-experimentation-talking-libraries-with-shannon-mattern-and-nate-hill/)

* __Medium, 7.2014__<br />
[Public Libraries | Multiple Solutions.](https://medium.com/@natenatenate/public-libraries-multiple-solutions-ced2107026be)

* __Chorus 'Gigatown' Contest, 7.2014__<br />
[The Public Library in the Gigabit Community: Chattanooga TN](http://gigatown.co.nz/blog/the-public-library-in-the-gigabit-community-chattanooga-tn/)



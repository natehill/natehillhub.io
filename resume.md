---
layout: page
title: Resume
permalink: /resume/
---

[Available here](/files/resume-9-2014.pdf) as a pdf file.

##Experience

###Chattanooga Public Library, Deputy Director, 2012- present
 
* Provide leadership, strategy, and direction for an agile, innovative, and internationally recognized library system that serves a population of almost 170,000 people in eastern Tennessee.
* Develop the annual operating budget of approximately $5.8 million for the library system, including a 3% funding increase for FY15 as a result of increased efficiencies, innovative new services, and the overall effective performance of the library.
* Scout and recruit world-class talent for an evolving, flexible library organization, resulting in agile, responsive service design as well as a library that is broadly recognized for its innovative projects and project teams.  Designate short-term cross-departmental project teams for rapid prototyping of new library services. 
* Instigated and implemented The 4th Floor library space.  The 4th Floor is a civic laboratory, makerspace, and beta-space with a focus on information, design, technology, and the applied arts. The 4th Floor supports the production, connection, and sharing of knowledge by offering access to tools and instruction.  The transformation of Chattanooga Public Library, its new reputation as a leading institution in the public library field, and the shift in the community’s perception of what makes a library an indispensible resource, are all a direct result of this ongoing project.
* Oversee and manage the daily operations of Digital Services, the 4th Floor Innovation Team, Technical Services, the Local History and Genealogy department, and other teams and divisions as needed.  Create new organizational structures and strategies frequently to respond to shifting priorities and customer needs.
* Develop and direct strategic partnerships with the mayor’s office and other city departments, local and national foundations, as well as other community organizations.  Serve on boards and advisory groups of other local organizations with related missions; thus bringing library in as a collaborator and partner.
* Approve media and communications that tell the library’s story, both to the library profession and to the community.  Frequently act as a public face for the library on local television, in the newspaper, in blogs and in magazines.  Write articles and other pieces to tell the library’s story. 

###San Jose Public Library, Web Librarian, 2009 - 2012

* Designed and implemented new public library website; migrated legacy content to open source platform. The site’s UI was successful and resulted in other public and academic libraries adapting or borrowing the style.
* Developed and deployed ‘Scan Jose’, an award winning responsive mobile web application featuring the library’s local history content.  In addition, the same content was made available via a third party augmented reality platform.

###Brooklyn Public Library, Branch Library Manager, Special Projects, Librarian 1999 - 2009
 
* Managed the Greenpoint Neighborhood Library; supervised a staff of 9 full time employees. Greenpoint is a busy, diverse urban neighborhood with a suitably busy library: in FY09 the library circulated 250K items and had a gate count of 290K visitors.
* Served on the Community Needs Assessment and Strategic Planning team, coordinated and ran focus groups and other input incubators for multilingual populations across neighborhood libraries in Brooklyn. Prepared and published reports that determined library service delivery strategies for a population of 2.5 million people.
* As a participant in the IMLS/Pratt Institute Public Urban Library Service Education (PULSE) program, earned the MLIS degree while rotating into different departments and locations across the Brooklyn Public Library. Over a tenure of almost ten years, held a variety of positions at nearly all of BPL’s 58 diverse branches.

##Board Appointments and Advisory Work

* Advisor, New York Architectural League / Center for an Urban Future Re-envisioning Branch Libraries project
* Board Member, Chattanooga Enterprise Center <br/> Under the leadership of Mayors Andy Berke and Jim Coppinger, the Enterprise Center is addressing three issues: Bridging the digital divide, Building on Chattanooga’s gigabit-per-second infrastructure, and the creation of an innovation district.
* Board Member, Causeway <br />Causeway is a Chattanooga-based non profit that provides support and resources to social and civic entrepreneurs who wish to improve the quality of life in Chattanooga.

##Grants, Awards, and Appointments

* Formerly, Bill and Melinda Gates Foundation International Network of Emerging Library Innovators
* Knight Foundation, 2013 Community Information Challenge grant
* Benwood Foundation, Innovation Fund for 4th Floor project, Award in partnership with Engage3D to host “devdev”, a summer technology and code camp for teens
* Digital Public Library of America, Co-Chair of Marketing and Outreach Committee
* IMLS, Project Director, “National Digital Summer Learning”
* Library Journal, “Mover and Shaker” award, 2012
* IMLS and NEH, Grant review panelist

##Consulting Clients

* Harvard University, Berkman Center for Internet and Society, Use case scenarios for Digital Public Library of America planning process
* Public Library Association (PLA), Blog Manager
* Influx Web UX Consulting, “One Pager” open source web page template for public libraries
* County of Los Angeles Public Library, CA, Library future visioning workshop
* The Dayton Metro Library, OH, Branding, UX, and futures workshops for $186 million building project

##Education

* Pratt Institute, New York, New York 2008 MLIS
* Skidmore College, Saratoga Springs, New York 1997 BS, Studio Art


